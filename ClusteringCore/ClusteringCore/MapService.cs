using System;
using System.Collections.Generic;
using ClusteringCore.MapUtils;
using AlgorithmClusterBundle = ClusteringCore.MapUtils.AlgorithmClusterBundle;


namespace ClusteringCore
{
    public class MapService : IMapService
    {
	    private const int RadiusDivider = 3;        
        private readonly AlgorithmClusterBundle _algorithmClusterBundle;

        public MapService()
        {
	        _algorithmClusterBundle = new AlgorithmClusterBundle();
        }       

        public List<Cluster> GetClustersForPoints(List<MapPoint> points, double mapRegionSpanInKm)
        {
	        try
	        {
		        var currentBufferDistance = (int)mapRegionSpanInKm / RadiusDivider;
            
		        var clusters = _algorithmClusterBundle.RunMarkerClusterer(points, currentBufferDistance);
            
		        return clusters;
	        }
	        catch (Exception e)
	        {
		        return new List<Cluster>();
	        }
        }
    }
}