using System;

namespace ClusteringCore.MapUtils
{
    public class MapPoint : Coordinates, IComparable
    {
        private const int _round = 6;
        public int Size { get; set; }

        public MapPoint(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public int CompareTo(object o)
        {
            if (Equals(o))
            {
                return 0;
            }

            var other = (MapPoint)o;
            if (Latitude > other.Latitude)
            {
                return -1;
            }

            if (Latitude < other.Latitude)
            {
                return 1;
            }

            return 0;
        }

        // used by k-means random distinct selection of cluster point
        public override int GetHashCode()
        {
            var x = Latitude * 10000;
            var y = Longitude * 10000;
            var r = x * 17 + y * 37;
            return (int)r;
        }

        public override bool Equals(Object o)
        {
            if (!(o is MapPoint other))
            {
                return false;
            }

            // rounding could be skipped
            // depends on granularity of wanted decimal precision
            // note, 2 points with same x,y is regarded as being equal
            var x = Math.Round(Latitude, _round) == Math.Round(other.Latitude, _round);
            var y = Math.Round(Longitude, _round) == Math.Round(other.Longitude, _round);
            return x && y;
        }
    }
}