using System.Collections.Generic;

namespace ClusteringCore.MapUtils
{
    public class Cluster
    {
        public string Id { get; set; }
        public MapPoint CenterPosition { get; set; }
        public List<MapPoint> Points { get; set; }
		
        private bool _isUsed;
        
        public bool IsUsed
        {
            get => _isUsed && CenterPosition != null;
            set => _isUsed = value;
        }

        public Cluster(string id)
        {
            IsUsed = true;
            CenterPosition = null;
            Points = new List<MapPoint>();
            Id = id;
        }
    }
}