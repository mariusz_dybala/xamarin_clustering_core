using System;
using System.Collections.Generic;

namespace ClusteringCore.MapUtils
{
    public partial class AlgorithmClusterBundle
    {
        public abstract class BaseClusterAlgorithm
        {
            public List<MapPoint> BaseDataset { get; set; }
            public readonly Dictionary<string, Cluster> BaseBucketsLookup = new Dictionary<string, Cluster>();
            
            public abstract List<Cluster> GetCluster(int bufferKm);
            
            public BaseClusterAlgorithm(List<MapPoint> dataset)
            {
                if (dataset == null || dataset.Count == 0)
                {
#if DEBUG
                    throw new Exception("Dataset for clustering is null or empty");
#endif  
                }

                BaseDataset = dataset;
            }
            
            public List<Cluster> BaseGetClusterResult()
            {
                var clusterPoints = new List<Cluster>();
                foreach (var item in BaseBucketsLookup)
                {
                    var bucket = item.Value;
                    if (bucket.IsUsed)
                    {
                        bucket.CenterPosition.Size = bucket.Points.Count;
                        clusterPoints.Add(bucket);
                    }
                }

                return clusterPoints;
            }

            // Assign all points to nearest cluster
            public void BaseUpdatePointsByCentroid()
            {
                // clear points in the buckets, they will be re-inserted
                foreach (var bucket in BaseBucketsLookup.Values)
                {
                    bucket.Points.Clear();
                }

                foreach (MapPoint p in BaseDataset)
                {
                    double minDistance = Double.MaxValue;
                    string index = string.Empty;
                    
                    foreach (var key in BaseBucketsLookup.Keys)
                    {
                        var bucket = BaseBucketsLookup[key];
                        if (bucket.IsUsed == false)
                        {
                            continue;
                        }

                        var centroid = bucket.CenterPosition;
                        var distance = MathTools.Distance(p, centroid);
                        if (distance < minDistance)
                        {
                            minDistance = distance;
                            index = key;
                        }
                    }

                    var closestBucket = BaseBucketsLookup[index];
                    closestBucket.Points.Add(p);
                }
            }
        }

    }
}