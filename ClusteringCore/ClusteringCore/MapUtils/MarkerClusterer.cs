using System.Collections.Generic;

namespace ClusteringCore.MapUtils
{
    public partial class AlgorithmClusterBundle
    {
        public class MarkerClusterer : BaseClusterAlgorithm
        {
            private const double BufferToClusterSizeFactor = 50;
            private double _markerClustererSize = 2;

            public MarkerClusterer(List<MapPoint> dataset)
                : base(dataset)
            {
            }

            public override List<Cluster> GetCluster(int bufferKm)
            {
                _markerClustererSize = (double) bufferKm / BufferToClusterSizeFactor;
                var cluster = RunClusterAlgorithm();
                return cluster;
            }

            private List<Cluster> RunClusterAlgorithm()
            {
                /*
               ITERATIVE LINEAR ADDING CLUSTER UNTIL 
               REPEATE iteration of clusters convergence 
                  until the max error is small enough
               if not, insert a new cluster at worst place 
               (farthest in region of worst cluster area) and run again 
               keeping current cluster points              
            
                // one iteration of clusters convergence is defined as ..
             1) Random k centroids
             2) Cluster data by euclidean distance to centroids
             3) Update centroids by clustered data,     
             4) Update cluster
             5) Continue last two steps until error is small, error is sum of diff
                 between current and updated centroid                          
              */
                
                int allPointsCount = BaseDataset.Count;
                var firstPoint = BaseDataset[0];
                var firstId = 0.ToString();
                
                var firstBucket = new Cluster(firstId) {CenterPosition = firstPoint};
                
                BaseBucketsLookup.Add(firstId, firstBucket);

                for (int i = 1; i < allPointsCount; i++)
                {
                    // Cluster candidate list.
                    var setWithIds = new HashSet<string>(); 
                    var responsePoint = BaseDataset[i];
                    // Iterate clusters and collect candidates.
                    foreach (var bucket in BaseBucketsLookup.Values)
                    {
                        var isInCluster = MathTools.BoxWithin(responsePoint, bucket.CenterPosition, _markerClustererSize);
                        if (!isInCluster)
                            continue;

                        setWithIds.Add(bucket.Id);
                        break;
                    }
                    if (setWithIds.Count == 0)
                    {
                        var pid = i.ToString();
                        var newbucket = new Cluster(pid) {CenterPosition = responsePoint};
                        BaseBucketsLookup.Add(pid, newbucket);
                    }
                }
                //Important. Align all points to closest cluster point.
                BaseUpdatePointsByCentroid();

                return BaseGetClusterResult();
            }
        }
    }
}