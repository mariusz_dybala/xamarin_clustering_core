using System.Collections.Generic;

namespace ClusteringCore.MapUtils
{
    public partial class AlgorithmClusterBundle
    {
        public List<Cluster> RunMarkerClusterer(List<MapPoint> points, int bufferKm)
        {
            return new MarkerClusterer(points).GetCluster(bufferKm);
        }
    }
}