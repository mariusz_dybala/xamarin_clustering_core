﻿using System.Collections.Generic;
using ClusteringCore.MapUtils;

namespace ClusteringCore
{
    internal class Program
    {
        public static void Main(string[] args)
        {
        
            var points = new List<MapPoint>
            {
                new MapPoint(39.299236, -76.609383), //Baltimore
                new MapPoint(38.9072, -77.03637), // Washington
                new MapPoint(40.7128, -74.0060), //NYC
                new MapPoint(39.9526, -75.1652), // Philadelphia
                new MapPoint(39.3643, -74.4229), // Atlantic City
                new MapPoint(37.5407, -77.4360), // Richmond
                new MapPoint(40.440624, -79.995888), // Pittsburgh 
                new MapPoint(43.653908, -79.384293), // Toronto
                new MapPoint(33.748783, -84.388168), // Atlanta
                new MapPoint(42.361145, -71.057083), // Boston
            };

            var mapService = new MapService();

            var clusters = mapService.GetClustersForPoints(points, 1000);
        }
    }
}