using System.Collections.Generic;
using ClusteringCore.MapUtils;

namespace ClusteringCore
{
    public interface IMapService
    {
        List<Cluster> GetClustersForPoints(List<MapPoint> points, double mapRegionSpanInKm);
    }
}