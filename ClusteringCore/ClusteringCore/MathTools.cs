using ClusteringCore.MapUtils;

namespace ClusteringCore
{
    public static class MathTools
    {
        public static double Distance(MapPoint a, MapPoint b)
        {
            return StraightLineDistance(a.Latitude, a.Longitude, b.Latitude, b.Longitude);
        }

        public static double StraightLineDistance(double latitudeFirst, double longitudeFirst, double latitudeSecond, double longitudeSecond)
        {
            var x = 69.1 * (latitudeFirst - latitudeSecond);
            var y = 69.1 * (longitudeFirst - longitudeSecond) * System.Math.Cos(latitudeSecond / 57.3);

            //calculation base : Miles
            var distance = System.Math.Sqrt(x * x + y * y);

            //Distance calculated in Kilometres
            return distance * 1.609;
        }

        public static bool BoxWithin(MapPoint a, MapPoint b, double boxsize)
        {
            var d = boxsize / 2;
            var withinX = a.Latitude - d <= b.Latitude && a.Latitude + d >= b.Latitude;
            var withinY = a.Longitude - d <= b.Longitude && a.Longitude + d >= b.Longitude;
            return withinX && withinY;
        } 
    }
}